#!/bin/bash
# automate install vim-plug by downloading it to autoload
# curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
#     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# automate make vim temp file
# mkdir ~/.vim/.tmp

ln -sf ~/Dotfiles/vim/.vimrc ~/.vimrc
ln -sf ~/Dotfiles/vim/ftplugin ~/.vim/

ln -sf ~/Dotfiles/bash/.bashrc ~/.bashrc
ln -sf ~/Dotfiles/tmux/.tmux.conf ~/.tmux.conf

ln -sf ~/Dotfiles/profile/.profile ~/.profile
